package pl.edu.pg.lab.device_stuart.common;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.core.UriBuilder;
import pl.edu.pg.lab.device_stuart.controller.ServerController;

public class UtilsCommon
{
    public static URI buildUri(Class<?> classType, String method, Object... vals)
    {
        return UriBuilder.fromPath("http://localhost:8080/+java-ee+/api")
            .path(classType)
            .path(classType, method)
            .build(vals);
    }

    public static URI buildUriWithParams(Class<?> classType, String method, int limit, int page,  Object... vals)
    {
        return UriBuilder.fromPath("http://localhost:8080/+java-ee+/api")
            .path(classType)
            .path(classType, method)
            .queryParam("limit", limit)
            .queryParam("page", page)
            .build(vals);
    }

    public static String lastPageFilterString(int page, int limit, int servers)
    {
        String nextPage = "Last Page";

        if(limit * page < servers)
            nextPage = UtilsCommon.buildUriWithParams(ServerController.class, "findAll", limit, page).toString();

        return nextPage;
    }

    public static String firstPageFilterString(int page, int limit)
    {
        String previousPage = "First Page";

        if(page > 0)
            previousPage = UtilsCommon.buildUriWithParams(ServerController.class, "findAll", limit, page-1).toString();

        return previousPage;
    }

    public static  <Type> List<Type> cutList(List<Type> list, int skip, int limit)
    {
        return list.stream()
            .skip(skip)
            .limit(limit)
            .collect(Collectors.toList());
    }
}