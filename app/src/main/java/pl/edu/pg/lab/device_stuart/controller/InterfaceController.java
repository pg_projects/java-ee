package pl.edu.pg.lab.device_stuart.controller;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.NoArgsConstructor;
import pl.edu.pg.lab.device_stuart.common.UtilsCommon;
import pl.edu.pg.lab.device_stuart.model.Interface;
import pl.edu.pg.lab.device_stuart.services.InterfaceService;

@ApplicationScoped
@NoArgsConstructor
@Path("/nics")
public class InterfaceController {

    @Inject
    InterfaceService nicService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public Response findAll() {
        List<Interface> nics = nicService.findAll();

        if(nics != null)
        {
            nics.forEach(nic -> nic.updateLinks("find"));

            return Response.ok(nics).build();
        }
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response find(@PathParam("id") int id) {
        Interface nic = nicService.find(id);

        if(nic != null)
        {
            nic.updateLinks("find");
            return Response.ok(nic).build();
        }
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add/")
    public Response add(@Valid Interface nic)
    {
       if(nic.getNumberId() == null)
       {
            nicService.save(nic);
            return Response.created(UtilsCommon.buildUri(InterfaceController.class, "find", nic.getNumberId())).build();
       }
       else
       {
            return Response.accepted().build();
       }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/edit/")
    public Response edit(@Valid Interface nicNew){
        Interface oldNic = nicService.find(nicNew.getNumberId());

        if(oldNic != null)
        {
             nicService.save(nicNew);
             return Response.created(UtilsCommon.buildUri(InterfaceController.class, "find", nicNew.getNumberId())).build();
        }
        else
        {
             return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response remove(@PathParam("id") int id){
        nicService.remove(id);

        return Response.ok().build();
    }
}
