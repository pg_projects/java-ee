package pl.edu.pg.lab.device_stuart.controller;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.NoArgsConstructor;
import pl.edu.pg.lab.device_stuart.common.UtilsCommon;
import pl.edu.pg.lab.device_stuart.model.Ipv4;
import pl.edu.pg.lab.device_stuart.services.Ipv4Service;

@ApplicationScoped
@NoArgsConstructor
@Path("/ipv4s")
public class Ipv4Controller{

    @Inject
    Ipv4Service ipv4Service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public Response findAll(@QueryParam("vlan") Integer vlan) {

        List<Ipv4> ipv4s;

        if(vlan != null)
            ipv4s = ipv4Service.filterByVlan(vlan);
        else
            ipv4s = ipv4Service.findAll();

        if(ipv4s != null)
        {
            return Response.ok(ipv4s).build();
        }
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{ip}")
    public Response find(@PathParam("ip") String address) {
        Ipv4 ipv4 = ipv4Service.find(address);

        if(ipv4 != null)
            return Response.ok(ipv4).build();
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add/")
    public Response add(@Valid Ipv4 ip)
    {
        Ipv4 oldIp = ipv4Service.find(ip.getAddress());

        if(oldIp == null)
        {
            try
            {
                ipv4Service.save(ip);
            }
            catch(ValidationException exception)
            {
                return Response.status(Response.Status.BAD_REQUEST)
                    .build();
            }

             return Response.created(UtilsCommon.buildUri(Ipv4Controller.class, "find", ip.getAddress())).build();
        }
        else
        {
             return Response.accepted().build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/edit/")
    public Response edit(@Valid Ipv4 ipNew)
    {
        Ipv4 ipOld = ipv4Service.find(ipNew.getAddress());

        if(ipOld != null)
        {
             ipv4Service.save(ipNew);
             return Response.created(UtilsCommon.buildUri(Ipv4Controller.class, "find", ipNew.getAddress())).build();
        }
        else
        {
             return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{address}")
    public Response remove(@PathParam("address") String address)
    {
        ipv4Service.remove(address);

        return Response.ok().build();
    }
}
