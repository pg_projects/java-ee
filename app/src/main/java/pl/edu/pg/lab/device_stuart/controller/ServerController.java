package pl.edu.pg.lab.device_stuart.controller;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.NoArgsConstructor;
import pl.edu.pg.lab.device_stuart.common.UtilsCommon;
import pl.edu.pg.lab.device_stuart.model.Server;
import pl.edu.pg.lab.device_stuart.services.DeviceService;

@ApplicationScoped
@NoArgsConstructor
@Path("/servers")
public class ServerController{

    @Inject
    DeviceService deviceService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public Response findAll(@QueryParam("limit") Integer limit, @QueryParam("page") Integer page)
    {
        List<Server> servers = (List<Server>)(List<?>)deviceService.findAll();
        int size = servers.size();
        String nextPage = "Not set";
        String previousPage = "Not set";

        if(servers != null)
        {
            if(limit != null && page != null)
            {
                servers = UtilsCommon.cutList(servers, page*limit, limit);
                previousPage = UtilsCommon.firstPageFilterString(page, limit);
                nextPage = UtilsCommon.lastPageFilterString(page+1, limit, size);
            }

            servers.forEach( server -> server.updateLinks("find"));

            return Response.ok(servers).header("Next-Page:", nextPage).header("Previous-Page", previousPage).build();
        }
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response find(@PathParam("id") int id)
    {
        Server server = (Server)deviceService.find(id);

        if(server != null)
        {
            server.updateLinks("find");
            return Response.ok(server).build();
        }
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response add(@Valid Server server)
    {
        if(server.getId() == null)
        {
            deviceService.save(server);
            return Response.created(UtilsCommon.buildUri(ServerController.class, "find", server.getId())).build();
       }
       else
       {
            return Response.accepted().build();
       }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/edit/")
    public Response edit(@Valid Server serverNew)
    {
        Server oldServer = (Server) deviceService.find(serverNew.getId());

        if(oldServer != null)
        {
             deviceService.save(serverNew);

             return Response.created(UtilsCommon.buildUri(ServerController.class, "find", serverNew.getId())).build();
        }
        else
        {
             return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response remove(@PathParam("id") int id){
        deviceService.remove(id);

        return Response.ok().build();
    }
}
