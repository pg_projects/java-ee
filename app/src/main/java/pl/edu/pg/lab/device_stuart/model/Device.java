package pl.edu.pg.lab.device_stuart.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public abstract class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer id;

    @NotEmpty(message = "Name for device cannot be empty.")
    private String name;

    @PastOrPresent(message = "Creation date cannot be from future.")
    private LocalDate creationDate = LocalDate.now();

    public Device(String name, int id)
    {
        this.id = id;
        this.name = name;
    }

    public Device(String name)
    {
        this.name = name;
    }

    public Device(Device device) {
        this.creationDate = device.creationDate;
        this.id = device.id;
        this.creationDate = device.creationDate;
    }
}
