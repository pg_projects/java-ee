package pl.edu.pg.lab.device_stuart.model;

import java.io.Serializable;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.edu.pg.lab.device_stuart.common.UtilsCommon;
import pl.edu.pg.lab.device_stuart.controller.Ipv4Controller;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Setter
@ToString(exclude = {"ips"})
@EqualsAndHashCode(exclude = {"ips"})
@NoArgsConstructor
@Table(name = "Interface")
@Entity
public class Interface implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer numberId;

    @NotEmpty(message = "Name for Interface cannot be empty.")
    private String name;

    @Size(min = 12, max = 12, message = "MAC address size must be equal 12.")
    private String macAddres;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private List<Ipv4> ips = new ArrayList<>();

    @PastOrPresent(message = "Creation date cannot be from future.")
    private LocalDate creationDate = LocalDate.now();

    @NotNull(message = "active must be true or false.")
    private boolean active;

    @Transient
    private List<URI> _embedded = new ArrayList<>();

    public Interface(int numberId, String name, String macAddres, LocalDate creationDate, boolean isActive)
    {
        this.numberId = numberId;
        this.name = name;
        this.creationDate = creationDate;
        this.macAddres = macAddres;
        this.active = isActive;
    }

    public Interface(String name, String macAddres, LocalDate creationDate, boolean isActive)
    {
        this.name = name;
        this.creationDate = creationDate;
        this.macAddres = macAddres;
        this.active = isActive;
    }

    public Interface(Interface device) {
        this.numberId = device.numberId ;
        this.name = device.name;
        this.creationDate = device.creationDate;
        this.macAddres = device.macAddres;
        this.active = device.active;
        this.ips = new ArrayList<>(device.ips);
    }

    public void removeIp(String address)
    {
        this.ips = this.ips.stream()
            .filter( ip -> !ip.getAddress().equals(address))
            .collect(Collectors.toList());
    }

    public void updateIp(String address, Ipv4 newIp)
    {
        removeIp(address);
        ips.add(newIp);
    }

    public void updateLinks(String method)
    {
        this._embedded = this.ips.stream()
            .map( ip ->
                UtilsCommon.buildUri(Ipv4Controller.class, method, ip.getAddress()))
            .collect(Collectors.toList());
    }
}
