package pl.edu.pg.lab.device_stuart.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.PositiveOrZero;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "Ipv4")
@NamedQueries({
    @NamedQuery(name="Ipv4.filterByVlan",
        query="SELECT ip FROM Ipv4 ip WHERE ip.vlan = :vlan"),
})
public class Ipv4 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @NotEmpty(message = "Address for Ipv4 cannot be empty.")
    private String address;

    @PositiveOrZero(message = "Vlan must be equal or greater then 0.")
    private int vlan;

    @PastOrPresent(message = "creationDate cannot be from future.")
    private LocalDate creationDate = LocalDate.now();

    public Ipv4(String address, int vlan)
    {
        this.vlan = vlan;
        this.address = address;
    }

    public Ipv4(Ipv4 ipv4) {
        this.address = ipv4.address;
        this.vlan = ipv4.vlan;
        this.creationDate = ipv4.creationDate;
    }
}
