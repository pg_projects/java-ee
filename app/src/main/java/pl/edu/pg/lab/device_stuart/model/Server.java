package pl.edu.pg.lab.device_stuart.model;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.edu.pg.lab.device_stuart.common.UtilsCommon;
import pl.edu.pg.lab.device_stuart.controller.InterfaceController;

@Data
@NoArgsConstructor
@ToString(exclude = {"nics"})
@EqualsAndHashCode(exclude = {"nics"})
@Entity(name = "Server")
public class Server extends Device
{
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "Place cannot be empty for Server.")
    String place;

    @NotEmpty(message = "Owner cannot be empty for Server.")
    String owner;

    @Positive(message = "Cpu count must be greater then 0.")
    int cpu;

    @PositiveOrZero(message = "Server load cannot be less then 0.")
    double loading;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REFRESH })
    private List<Interface> nics = new ArrayList<>();

    @Transient
    @JsonbTransient
    private List<URI> _embedded = new ArrayList<>();

    @Transient
    private URI nextPage;

    @Transient
    private URI previousPage;

    public Server(int id, String name, String place, String owner, int cpu, float loading)
    {
      super(name,id);

      this.place = place;
      this.owner = owner;
      this.cpu = cpu;
      this.loading = loading;
    }

    public Server(String name, String place, String owner, int cpu, float loading)
    {
      super(name);

      this.place = place;
      this.owner = owner;
      this.cpu = cpu;
      this.loading = loading;
    }

    public Server(Server server)
    {
        super((Device)(server));
        this.place = server.place;
        this.owner = server.owner;
        this.cpu = server.cpu;
        this.loading = server.loading;
        this.nics = server.nics;
    }

    public void updateLinks(String method)
    {
        this._embedded = this.nics.stream()
            .map( nic ->
                UtilsCommon.buildUri(InterfaceController.class, method, nic.getNumberId()))
            .collect(Collectors.toList());

        this.nics.stream().forEach( nic ->
            nic.updateLinks(method));
    }
}
