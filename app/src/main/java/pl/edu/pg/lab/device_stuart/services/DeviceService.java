package pl.edu.pg.lab.device_stuart.services;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import lombok.NoArgsConstructor;
import pl.edu.pg.lab.device_stuart.model.Device;
import pl.edu.pg.lab.device_stuart.model.Server;

@ApplicationScoped
@NoArgsConstructor
@Named("DeviceService")
public class DeviceService {

    @PersistenceContext(unitName= "nics")
    private EntityManager entityManager;

    public List<Device> findAll()
    {
        Query query = entityManager.createQuery("SELECT server FROM Server server");

        return query.getResultList();
    }

    @Transactional
    public Device find(int id)
    {
        return entityManager.find(Server.class, id);
    }

    @Transactional
    public void save(Device newDevice)
    {
        if(newDevice.getId() != null)
        {
            this.edit(newDevice);
        }
        else
            add(newDevice);
    }

    private void add(Device device)
    {
        entityManager.persist(device);
    }

    private void edit(Device device)
    {
        entityManager.merge(device);
    }

    @Transactional
    public void remove(int id){
        entityManager.remove(
            entityManager.merge(
                this.find(id)));
  }
}