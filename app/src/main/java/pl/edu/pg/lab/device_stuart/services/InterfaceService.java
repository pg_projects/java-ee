package pl.edu.pg.lab.device_stuart.services;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import lombok.NoArgsConstructor;
import pl.edu.pg.lab.device_stuart.model.Interface;

@ApplicationScoped
@NoArgsConstructor
@Named("InterfaceService")
public class InterfaceService {

    @PersistenceContext(unitName= "nics")
    private EntityManager entityManager;

    public List<Interface> findAll() {
        Query query = entityManager.createQuery("SELECT nic FROM Interface nic");

        return query.getResultList();
    }

    public Interface find(int id) {
        return entityManager.find(Interface.class, id);
    }

    @Transactional
    public void save(Interface newNic)
    {
        if (newNic.getNumberId() != null)
        {
            this.edit(newNic);
        }
        else
            add(newNic);
    }


    private void add(Interface nic)
    {
        entityManager.persist(nic);
    }


    private void edit(Interface nic)
    {
        entityManager.merge(nic);
    }

    @Transactional
    public void remove(int id)
    {
        entityManager.remove(
            entityManager.merge(
                this.find(id)));
    }
}
