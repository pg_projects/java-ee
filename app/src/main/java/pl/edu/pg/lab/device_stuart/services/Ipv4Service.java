package pl.edu.pg.lab.device_stuart.services;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import lombok.NoArgsConstructor;
import pl.edu.pg.lab.device_stuart.model.Ipv4;

@ApplicationScoped
@NoArgsConstructor
@Named("Ipv4Service")
public class Ipv4Service
{
    @PersistenceContext(unitName= "nics")
    private EntityManager entityManager;

    public List<Ipv4> findAll() {
        Query query = entityManager.createQuery("SELECT e FROM Ipv4 e");
        return query.getResultList();
    }

    @Transactional
    public Ipv4 find(String address) {
        return entityManager.find(Ipv4.class, address);
    }

    @Transactional
    public void save(Ipv4 newIp)
    {
        Ipv4 oldIp = this.find(newIp.getAddress());

        if (oldIp != null)
            this.edit(newIp);
        else
            add(newIp);
    }


    private void add(Ipv4 ip)
    {
        entityManager.persist(ip);
    }


    private void edit(Ipv4 newIp)
    {
        entityManager.merge(newIp);
    }

    @Transactional
    public void remove(String address){
        Ipv4 toDelete = this.find(address);

        entityManager.remove(
            entityManager.merge(toDelete));
  }

  public List<Ipv4> filterByVlan(int vlan)
  {
      return entityManager.createNamedQuery(
          "Ipv4.filterByVlan", Ipv4.class)
          .setParameter("vlan", vlan)
          .getResultList();
  }
}
