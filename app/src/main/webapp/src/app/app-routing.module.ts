import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponentComponent  } from './page-not-found-component/page-not-found-component.component';


const routes: Routes = [
  { path: '',
    redirectTo: '/ipv4/all',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponentComponent  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
      {enableTracing: true}
      )],
  exports: [RouterModule],
})
export class AppRoutingModule { }
