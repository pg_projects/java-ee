import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import { NicModule } from './nic/nic.module';
import { Ipv4Module } from './ipv4/ipv4.module';
import { Ipv4Service } from './ipv4/ipv4.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponentComponent,
  ],
  imports: [
    BrowserModule,
    NicModule,
    Ipv4Module,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [ Ipv4Service ],
  bootstrap: [AppComponent]
})
export class AppModule { }
