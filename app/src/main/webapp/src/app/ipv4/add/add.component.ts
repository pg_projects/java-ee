import { Component, OnInit } from '@angular/core';
import { Ipv4Service } from '../ipv4.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ipv4 } from '../ipv4';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  ipForm: FormGroup;
  private ip: Ipv4;
  private addr: string
  private edit_mode: boolean = false;

  constructor(
    private ipService: Ipv4Service,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) { }

    ngOnInit(){
      this.getAddress();

      if(this.edit_mode){
        this.edit_mode = true;
        this.ipService.get(this.addr).subscribe( ip => {
          this.initForm(ip)})
      }
      else {
        this.initForm();
      }
  }

  save(){
    console.log(`Got from form ${JSON.stringify(this.ipForm.value)}`)
    this.ip = new Ipv4(this.ipForm.value);

    if(this.edit_mode)
      this.ipService.edit(this.ip);
    else
      this.ipService.save(this.ip);

    this.router.navigate(['/ipv4/all']);
  }

  private initForm(ip:Ipv4 = new Ipv4()){
    this.ipForm = this.fb.group({
      address: [`${ip.address}`],
      vlan: [`${ip.vlan}`],
      creationDate:  [`${ip.creationDate}`]
    });
  }

  private getAddress(){
      return this.route.paramMap.subscribe(params => {
        var addr = params.get("address");

        if(addr){
          console.log(`Got address: ${addr}`);
          this.addr = addr;
          this.edit_mode = true;
        }
      });
    }
}
