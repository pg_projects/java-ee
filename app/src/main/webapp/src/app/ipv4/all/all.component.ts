import { Component, OnInit } from '@angular/core';
import { Ipv4Service } from '../ipv4.service';
import { Ipv4 } from '../ipv4';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  ips:Ipv4[];
  isDataAvailable:boolean = false;

  constructor(
    private ipService: Ipv4Service,
    private router: Router) {}

  ngOnInit() {
    this.fillTable();
  }

  private fillTable() {
    this.ipService.getAll()
      .subscribe(ips =>{
        console.log(`Got ips from server: ${ips}`);
        this.ips = ips;
        this.isDataAvailable = true;
      }
    );
  }

  delete(address:string) {
    this.ipService.remove(address);
    this.ips = this.ips.filter(ip => ip.address !== address);
    this.router.navigate(['/ipv4/all']);
  }
}
