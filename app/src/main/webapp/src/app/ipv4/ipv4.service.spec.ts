import { TestBed } from '@angular/core/testing';

import { Ipv4Service } from './ipv4.service';

describe('Ipv4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Ipv4Service = TestBed.get(Ipv4Service);
    expect(service).toBeTruthy();
  });
});
