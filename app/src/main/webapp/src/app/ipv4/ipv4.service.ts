import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ipv4 } from './ipv4';

@Injectable({
  providedIn: 'root'
})

export class Ipv4Service {
  private base_url = 'http://localhost:8080/+java-ee+/api/ipv4s'

  constructor(private http: HttpClient) {}
    getAll(): Observable<Ipv4[]> {
      var url = `${this.base_url}/get/all`
      return this.http.get<Ipv4[]>(url);
    }

    get(address:string): Observable<Ipv4>{
      var url = `${this.base_url}/get/${address}`
      console.log(url)
      return this.http.get<Ipv4>(url);
    }

    edit(ip:Ipv4): void {
      var url = `${this.base_url}/edit/`
      var json = JSON.stringify(ip)
      console.log(`Send ${json } to ${url}`)

      this.http.put(url, ip)
        .subscribe((response: Response) =>{
          if(response)
            console.log(response.json());
        });
    }

    remove(address:string): void {
      var url = `${this.base_url}/delete/${address}`
      console.log(`Send ${address} to ${url}`)

      this.http.delete(url)
        .subscribe((response: Response) =>{
          if(response)
            console.log(response.json());
        });
    }

    save(ip:Ipv4): void {
      var url = `${this.base_url}/add/`
      var json = JSON.stringify(ip)
      console.log(`Send ${json } to ${url}`)

      this.http.post(url, ip)
        .subscribe((response: Response) =>{

          if(response)
            console.log(response.json());
        });
    }
}