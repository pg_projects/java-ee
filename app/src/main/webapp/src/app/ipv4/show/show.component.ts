import { Component, OnInit } from '@angular/core';
import { Ipv4 } from '../ipv4';
import { Ipv4Service } from '../ipv4.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  constructor(
    private ipService: Ipv4Service,
    private route: ActivatedRoute) { }

  ip:Ipv4;
  selectedIp:string;
  isDataAvailable:boolean = false;

  ngOnInit() {
    this.selectIp()
    this.fillTable()
  }

  private fillTable() {
    console.log(`Generate table for ip: ${this.selectedIp}`);
    this.ipService.get(this.selectedIp)
      .subscribe(ip =>
      {
        console.log(`Got ip: ${ip}`);
        this.ip = ip;
        this.isDataAvailable = true;
      }
    );
  }

  private selectIp(){
    return this.route.paramMap.subscribe(params => {
      var addr = params.get("address");
      console.log(`Got address: ${addr}`);
      this.selectedIp = addr;
    })
  }
}
