import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Nic } from '../nic';
import { NicService } from '../nic.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Ipv4Service } from 'src/app/ipv4/ipv4.service';
import { Ipv4 } from 'src/app/ipv4/ipv4';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  availableIps:Ipv4[];
  nicForm: FormGroup;
  selectedIps:Ipv4[] = [];
  dataIsAvailable:boolean = false;

  private nic: Nic;
  private id: number
  private edit_mode: boolean = false;

  constructor(
    private nicService: NicService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private ipService: Ipv4Service)
    {
      this.getIps();
    }

  ngOnInit() {
    this.getId();

    if(this.edit_mode)
    {
      this.nicService.get(this.id).subscribe( nic => {
        this.initForm(nic)})
    }
    else
    {
      this.initForm();
    }
      this.dataIsAvailable = true;
  }

  save()
  {
    console.log(`Got from form ${JSON.stringify(this.nicForm.value)}`)
    this.nic = new Nic(this.nicForm.value);

    if(this.edit_mode)
      this.nicService.edit(this.nic);
    else
      this.nicService.save(this.nic);

    this.router.navigate(['/nics/all']);
  }

  private initForm(nic:Nic = new Nic())
  {
    console.log("Init add form")

    this.nicForm = this.fb.group(
    {
      numberId: [`${nic.numberId}`],
      name: [`${nic.name}`],
      macAddres:  [`${nic.macAddres}`],
      active: [`${nic.active}`],
      creationDate: [`${nic.creationDate}`],
      ips: [`${nic.ips}`]
    });
  }

  private getId(){
      return this.route.paramMap.subscribe(params => {
        var id = params.get("id");

        if(id)
        {
          console.log(`Got address: ${id}`);
          this.id = Number(id);
          this.edit_mode = true;
        }
      });
    }

  public onChange(newobj)
  {
    this.selectedIps.push(newobj);
  }

  public getIps(){
    this.ipService.getAll().subscribe( ips =>
      {
        console.log('Got available ips from server')
        this.availableIps = ips
      });
  }
}
