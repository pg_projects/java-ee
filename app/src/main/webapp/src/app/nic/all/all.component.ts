import { Component, OnInit } from '@angular/core';
import { NicService } from '../nic.service';
import { Nic } from '../nic';
import { Router } from '@angular/router';
import { Ipv4 } from 'src/app/ipv4/ipv4';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  nics:Nic[];
  isDataAvailable:boolean = false;

  constructor(private nicService:NicService, private router: Router) { }

  ngOnInit() {
    this.fillTable();
  }

  private fillTable() {
    this.nicService.getAll()
      .subscribe(nics =>
      {
        this.nics = nics;
        this.isDataAvailable = true;
        console.log(`Got nics from server: ${JSON.stringify(nics)}`);
      }
    );
  }

  delete(id:number) {
    this.nicService.remove(id);
    this.nics = this.nics.filter(nic => nic.numberId !== id);
    this.router.navigate(['/nics/all']);
  }

}
