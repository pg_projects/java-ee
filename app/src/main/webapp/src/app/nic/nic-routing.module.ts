import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';
import { AllComponent } from './all/all.component';

const routes: Routes = [
  { path: 'nics/add', component: AddComponent },
  { path: 'nics/all', component: AllComponent },
  { path: 'nics/edit/:id', component: AddComponent },
  { path: 'nics/show/:id', component: ShowComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NicRoutingModule { }
