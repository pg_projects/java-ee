import { TestBed } from '@angular/core/testing';

import { NicService } from './nic.service';

describe('NicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NicService = TestBed.get(NicService);
    expect(service).toBeTruthy();
  });
});
