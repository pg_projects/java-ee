import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Nic } from './nic';

@Injectable({
  providedIn: 'root'
})
export class NicService {
  constructor(private http: HttpClient) {
  }
      private api_url:string = 'http://localhost:8080/+java-ee+/api/nics/';

      getAll(): Observable<Nic[]> {
      var url = `${this.api_url}get/all`
      return this.http.get<Nic[]>(url);
    }

    get(id:number): Observable<Nic>{
      var url = `${this.api_url}get/${id}`
      console.log(url)
      return this.http.get<Nic>(url);
    }

    edit(nic:Nic): void {
      var url = `${this.api_url}edit/`
      var json = JSON.stringify(nic)
      console.log(`Send ${json } to ${url}`)

      this.http.put(url, nic)
        .subscribe((response: Response) =>{
          if(response)
            console.log(response.json());
        });
    }

    remove(id:number): void {
      var url = `${this.api_url}delete/${id}`
      console.log(`Send ${id} to ${url}`)

      this.http.delete(url)
        .subscribe((response: Response) =>{
          if(response)
            console.log(response.json());
        });
    }

    save(nic:Nic): void {
      var url = `${this.api_url}add/`
      var json = JSON.stringify(nic)
      console.log(`Send ${json } to ${url}`)

      this.http.post(url, nic)
        .subscribe((response: Response) =>{
          if(response)
            console.log(response.json());
        });
    }
}
