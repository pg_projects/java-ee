// package pl.edu.pg.lab.device_stuart.services;

// import java.time.LocalDate;
// import java.util.Random;
// import java.util.stream.Collectors;
// import javax.annotation.PostConstruct;
// import javax.enterprise.context.ApplicationScoped;
// import javax.inject.Inject;
// import javax.inject.Named;

// import lombok.NoArgsConstructor;
// import pl.edu.pg.lab.device_stuart.model.Interface;
// import pl.edu.pg.lab.device_stuart.model.Ipv4;
// import pl.edu.pg.lab.device_stuart.model.Server;

// @ApplicationScoped
// @NoArgsConstructor
// @Named("DataSeeder")
// public class DataSeeder
// {
//     @Inject
//     private DeviceService serverService;

//     @Inject
//     private InterfaceService interfaceService;

//     @Inject
//     private Ipv4Service ipService;

//     String[] ipAddr =
//     {
//       "118.71.167.53",
//       "46.20.134.211",
//       "196.113.22.68",
//       "14.165.83.48",
//       "194.209.55.160",
//       "122.236.131.172",
//       "129.188.25.40",
//       "65.244.74.164",
//       "115.194.97.157",
//       "3.23.140.230",
//       "155.1.5.114",
//       "177.209.181.32",
//       "255.116.163.94",
//       "33.186.228.144",
//       "245.223.6.40",
//       "76.194.169.38",
//       "190.230.149.205",
//       "122.24.206.240",
//       "117.151.106.167",
//       "254.76.245.189",
//       "222.6.228.165",
//       "178.196.234.7",
//       "192.128.146.200",
//       "251.129.201.195",
//       "161.168.109.238",
//       "43.21.130.20",
//       "193.221.53.141",
//       "57.214.220.90",
//       "17.203.196.223",
//       "205.16.232.79"
//     };

//     int[] vlans =
//     {
//       48,
//       91,
//       82,
//       54,
//       10,
//       47,
//       25,
//       6,
//       94,
//       81,
//       79,
//       96,
//       68,
//       55,
//       51,
//       63,
//       19,
//       96,
//       76,
//       70,
//     };

//     String[] macs =
//     {
//       "58:aa:7c:04:e3:41",
//       "3c:80:75:46:2f:0c",
//       "98:a3:17:99:9e:35",
//       "a0:ab:7b:ed:56:a0",
//       "8f:ab:e6:b6:c7:8c",
//       "e2:f8:0b:e8:0e:09",
//       "6e:08:04:4f:a2:89",
//       "98:bd:69:3a:b4:5f",
//       "47:11:f6:b6:27:c6",
//       "9b:20:57:9e:03:13",
//       "9e:35:f2:1e:3b:6f",
//       "0f:38:f0:02:87:b7",
//       "bf:58:d8:f5:48:bd",
//       "99:a9:1f:65:9e:32",
//       "56:27:a8:38:cd:26",
//       "24:65:ab:4f:a3:07",
//       "4d:80:64:66:08:d5",
//       "f5:1d:36:28:42:b1",
//       "89:eb:ae:26:85:63",
//       "0b:05:4a:0b:16:19",
//     };

//     String[] nicNames =
//     {
//       "eth1",
//       "eth2",
//       "eth3",
//       "eth4",
//       "eth5",
//       "eth6",
//       "eth7",
//       "eth8",
//       "eth9",
//       "eth10",
//       "eth11",
//       "eth12",
//       "eth13",
//       "eth14",
//       "eth15"
//     };

//     LocalDate[] dates =
//     {
//       LocalDate.parse("2006-04-07"),
//       LocalDate.parse("2019-07-10"),
//       LocalDate.parse("2011-02-03"),
//       LocalDate.parse("2013-02-14"),
//       LocalDate.parse("2015-07-05"),
//       LocalDate.parse("2008-07-06"),
//       LocalDate.parse("2017-12-09"),
//       LocalDate.parse("2019-05-15"),
//       LocalDate.parse("2017-01-10"),
//       LocalDate.parse("2018-09-10"),
//       LocalDate.parse("2011-10-26"),
//       LocalDate.parse("2000-05-14"),
//       LocalDate.parse("2010-01-26"),
//       LocalDate.parse("2008-04-08"),
//       LocalDate.parse("2006-06-01"),
//       LocalDate.parse("2011-01-27"),
//       LocalDate.parse("2006-05-14"),
//       LocalDate.parse("2001-04-27"),
//       LocalDate.parse("2013-09-29"),
//       LocalDate.parse("2000-04-21")
//     };

//     String[] serversName =
//     {
//       "Martha",
//       "Theodora",
//       "Achilleas",
//       "Gianna",
//       "Myrto",
//       "Ignatios",
//       "Panos",
//       "Mihail",
//       "Thekla",
//       "Xanthippi",
//       "Demetra",
//       "Yannis",
//       "Athina",
//       "Ignatios",
//       "Ermis"
//     };

//     String[] cities =
//     {
//       "Pantmawr",
//       "Hewe",
//       "Westray",
//       "Rachdale",
//       "Berkton",
//       "Chepstow",
//       "Arbington",
//       "Peltragow",
//       "Aermagh",
//       "Larton"
//     };

//     String[] owners =
//     {
//       "Norris Hutchinson",
//       "Una Webster",
//       "Ward Byrd",
//       "Buck Binder",
//     };

//     int[] cpus =
//     {
//       34,
//       37,
//       37,
//       34,
//       34,
//       35,
//       36,
//       33,
//       37,
//       34
//     };

//     float[] loads =
//     {
//       0.34F,
//       0.37F,
//       0.37F,
//       0.34F,
//       0.34F,
//       0.35F,
//       0.36F,
//       0.33F,
//       0.37F,
//       0.34F
//     };

//     static int nicId = 0;
//     static int serverId = 0;

//     private Ipv4 randomIP()
//     {
//       String addr = ipAddr[(new Random()).nextInt(ipAddr.length)];
//       int vlan = vlans[(new Random()).nextInt(vlans.length)];

//       return new Ipv4(addr, vlan);
//     }

//     private Interface randomNic()
//     {
//       String nicName = nicNames[(new Random()).nextInt(nicNames.length)];
//       String mac = macs[(new Random()).nextInt(macs.length)];
//       LocalDate date=dates[(new Random()).nextInt(dates.length)];
//       boolean activ = new Random().nextBoolean();
//       int start = new Random().nextInt(ipService.findAll().size());
//       int end = new Random().nextInt(ipService.findAll().size()) % start;

//       Interface nic = new Interface(nicId++,nicName, mac, date, activ);

//       nic.setIps(ipService.findAll().stream()
//         .skip(start)
//         .limit(end)
//         .collect(Collectors.toList()));

//       return nic;
//     }

//     private Server randomServer()
//     {
//       String name = serversName[(new Random()).nextInt(serversName.length)];
//       String city = cities[(new Random()).nextInt(cities.length)];
//       String owner = owners[(new Random()).nextInt(owners.length)];
//       int cpu = cpus[(new Random()).nextInt(cpus.length)];
//       float load = loads[(new Random()).nextInt(loads.length)];

//       int start = new Random().nextInt(interfaceService.findAll().size());
//       int end = new Random().nextInt(interfaceService.findAll().size()) % start;

//       Server server = new Server(serverId++, name, city, owner, cpu, load);

//       server.setNics(interfaceService.findAll().stream()
//           .skip(start)
//           .limit(end)
//           .collect(Collectors.toList()));

//       return server;
//     }

//     @PostConstruct
//     void seed()
//     {
//       for(int i=0; i<20; i++)
//           ipService.save(randomIP());

//       for(int i=0; i<20; i++)
//           interfaceService.save(randomNic());

//       for(int i=0; i<20; i++)
//           serverService.save((Server)randomServer());
//     }
// }
