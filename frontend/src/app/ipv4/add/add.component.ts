import { Component, OnInit } from '@angular/core';
import { Ipv4Service } from '../ipv4.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ipv4 } from '../ipv4';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  ipForm: FormGroup;
  private ip: Ipv4;
  private addr: string
  private edit_mode: boolean = false;

  constructor(
    private ipService: Ipv4Service,
    private fb: FormBuilder,
    private route: ActivatedRoute) { }

    ngOnInit(){
      this.getAddress();

      if(this.edit_mode){
        this.edit_mode = true;
        this.ipService.get(this.addr).subscribe( ip => {
          this.initForm(ip)})
      }
      else {
        this.initForm();
      }
  }

  updateField(name, error)
  {
    this.ipForm
      .controls[name]
        .setErrors({'incorrect': true, 'message': error});

    if(name == 'vlan')
      this.vlanError=error;

    }

  save(){

    this.ip = new Ipv4(this.ipForm.value);

    var fields = ['vlan', 'creationDate', 'address'];

    let response:any = null;

    if(this.edit_mode)
      response = this.ipService.edit(this.ip);
    else
      response = this.ipService.save(this.ip);

      console.log(response);
      if(response !=null) {
        console.log(response['parameterViolations']) // tablica slownikow szukac w path i odbierac message

        for( let field in fields) {
          for(let [key, value] of Object.entries(response)) {
            if(field in value['path'])
            this.updateField(field, value['message'])
          }
        }
      }

    console.log(this.vlanError);
    // this.router.navigate(['/ipv4/all']);
  }

  vlanError:string='';
  private initForm(ip:Ipv4 = new Ipv4()){
    this.ipForm = this.fb.group({
      address: [`${ip.address}`],
      vlan: [`${ip.vlan}`],
      creationDate:  [`${ip.creationDate}`]
    });
  }

  private getAddress(){
      return this.route.paramMap.subscribe(params => {
        var addr = params.get("address");

        if(addr){
          console.log(`Got address: ${addr}`);
          this.addr = addr;
          this.edit_mode = true;
        }
      });
    }
}
