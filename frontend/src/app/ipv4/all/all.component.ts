import { Component, OnInit } from '@angular/core';
import { Ipv4Service } from '../ipv4.service';
import { Ipv4 } from '../ipv4';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  ips:Ipv4[];
  isDataAvailable:boolean = false;
  filterForm:FormControl = new FormControl('');

  constructor(
    private ipService: Ipv4Service,
    private router: Router) {}

  ngOnInit() {
    this.fillTable();
  }

  private filter() {
    this.fillTable(this.filterForm.value ? this.filterForm.value : -1);
  }
  private fillTable(vlan:number=-1) {
    this.ipService.getAll(vlan)
      .subscribe(ips =>{
        this.ips = ips;
        this.isDataAvailable = true;
      });

    this.router.navigate(['/ipv4/all']);
  }

  delete(address:string) {
    this.ipService.remove(address);
    this.ips = this.ips.filter(ip => ip.address !== address);
    this.router.navigate(['/ipv4/all']);
  }
}
