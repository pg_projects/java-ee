import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { AllComponent } from './all/all.component';
import { ShowComponent } from './show/show.component';

const routes: Routes = [
  { path: 'ipv4/add', component: AddComponent },
  { path: 'ipv4/edit/:address', component: AddComponent },
  { path: 'ipv4/all', component: AllComponent },
  { path: 'ipv4/show/:address', component: ShowComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Ipv4RoutingModule { }
