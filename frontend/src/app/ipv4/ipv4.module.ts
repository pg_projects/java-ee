import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Ipv4RoutingModule } from './ipv4-routing.module';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';
import { AllComponent } from './all/all.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddComponent,
    AllComponent,
    ShowComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    Ipv4RoutingModule
  ]
})
export class Ipv4Module { }
