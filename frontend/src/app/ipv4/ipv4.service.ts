import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ipv4 } from './ipv4';

@Injectable({
  providedIn: 'root'
})

export class Ipv4Service {
  private base_url = 'http://backend:8080/+java-ee+/api/ipv4s'

    constructor(private http: HttpClient) {}

    getAll(vlan:number=-1): Observable<Ipv4[]> {
      var url = `${this.base_url}/all`
      let params = new HttpParams();

      if(vlan != -1)
          params = params.append('vlan', vlan.toString());

      return this.http.get<Ipv4[]>(url, {params: params});
    }

    get(address:string): Observable<Ipv4> {
      var url = `${this.base_url}/${address}`

      return this.http.get<Ipv4>(url);
    }

    edit(ip:Ipv4): Observable<Object> {
      var url = `${this.base_url}/edit/`

      return this.http.put(url, ip);
    }

    remove(address:string): void {
      var url = `${this.base_url}/${address}`

      this.http.delete(url)
        .subscribe((response: Response) =>{
        });
    }

    save(ip:Ipv4): void {
      var url = `${this.base_url}/add/`

      this.http.post(url, ip)
    }
}