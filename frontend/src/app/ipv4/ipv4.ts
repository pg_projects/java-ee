export class Ipv4 {
  address:string = '';
  vlan:number = 0;
  creationDate:Date = new Date("2019-10-19");

  constructor(ip?: Partial<Ipv4>){
    if(ip)
      Object.assign(this,ip);
  }
}
