import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Nic } from '../nic';
import { NicService } from '../nic.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Ipv4Service } from 'src/app/ipv4/ipv4.service';
import { Ipv4 } from 'src/app/ipv4/ipv4';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  availableIps:Ipv4[];
  nicForm: FormGroup;
  selectedIps:Ipv4[] = [];
  dataIsAvailable:boolean = false;

  private nic: Nic;
  private id: number
  private editMode: boolean = false;

  constructor(
    private nicService: NicService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private ipService: Ipv4Service)
    {}

  ngOnInit() {
    this.getAvailableIps();
    this.getId();

    this.initForm();

    if(this.editMode) {
      this.nicService.get(this.id).subscribe( nic => {
        this.updateForm(nic);
      });
    }

      this.dataIsAvailable = true;
  }

  save()
  {
    console.log(`Got from form ${JSON.stringify(this.nicForm.value)}`)
    this.nic = new Nic(this.nicForm.value);

    if (this.nic.ips == null)
      this.nic.ips = [];

    if(this.editMode) {
      this.nic.numberId = this.id;
      this.nicService.edit(this.nic);
    }
    else
      this.nicService.save(this.nic);

    this.router.navigate(['/nics/all']);
  }

  private initForm()
  {
    this.nicForm = this.fb.group( {
      name: new FormControl('', Validators.required),
      macAddres:  new FormControl('', Validators.required),
      active: new FormControl('', Validators.required),
      creationDate: new FormControl('', Validators.required),
      ips: new FormControl(null),
    });
  }

  private updateForm(nic:Nic = new Nic())
  {
    this.nicForm = this.fb.group( {
      name: [`${nic.name}`],
      macAddres:  [`${nic.macAddres}`],
      active: [`${nic.active}`],
      creationDate: [`${nic.creationDate}`],
      ips: [`${nic.ips}`]
    });
  }

  private getId(){
      return this.route.paramMap.subscribe(params => {
        var id = params.get("id");

        if(id)
        {
          this.id = Number(id);
          this.editMode = true;
        }
      });
    }

  public onChange(newobj)
  {
    this.selectedIps.push(newobj);
  }

  public getAvailableIps(){
    this.ipService.getAll().subscribe( ips =>
      {
        console.log('Got available ips from server')
        this.availableIps = ips
      });
  }
}
