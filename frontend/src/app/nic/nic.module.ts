import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NicRoutingModule } from './nic-routing.module';
import { ShowComponent } from './show/show.component';
import { AddComponent } from './add/add.component';
import { AllComponent } from './all/all.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddComponent,
    AllComponent,
    ShowComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    NicRoutingModule
  ]
})
export class NicModule { }
