import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Nic } from './nic';

@Injectable({
  providedIn: 'root'
})
export class NicService {
  constructor(private http: HttpClient) {
  }
      private api_url:string = 'http://backend:8080/+java-ee+/api/nics/';

      getAll(): Observable<Nic[]> {
      var url = `${this.api_url}all`
      return this.http.get<Nic[]>(url);
    }

    get(id:number): Observable<Nic>{
      var url = `${this.api_url}${id}`
      return this.http.get<Nic>(url);
    }

    edit(nic:Nic): void {
      var url = `${this.api_url}edit/`

      this.http.put(url, nic)
        .subscribe((response: Response) =>{
          if(response)
          console.log(response.json());
        });
    }

    remove(id:number): void {
      var url = `${this.api_url}${id}`

      this.http.delete(url)
        .subscribe((response: Response) =>{
        });
    }

    save(nic:Nic): void {
      var url = `${this.api_url}add/`

      this.http.post(url, nic)
        .subscribe((response: Response) =>{
        });
    }
}
