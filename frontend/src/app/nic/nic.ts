import { Ipv4 } from '../ipv4/ipv4';

export class Nic {
  name:string = '';
  macAddres:string = '';
  ips:Ipv4[] = [];
  active:boolean = false;
  creationDate:Date = new Date("2019-10-19");
  numberId:number;

  constructor(nic?: Partial<Nic>){
    if(nic)
      Object.assign(this, nic);
  }
}