import { Component, OnInit } from '@angular/core';
import { Nic } from '../nic';
import { NicService } from '../nic.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

constructor(private nicService: NicService, private route: ActivatedRoute) { }
  nic:Nic;
  selectedNic:number;
  isDataAvailable:boolean = false;

  ngOnInit() {
    this.selectIp()
    this.fillTable()
  }

  private fillTable() {
    console.log(`Generate table for nic: ${this.selectedNic}`);
    this.nicService.get(this.selectedNic)
      .subscribe(nic =>
      {
        console.log(`Got nic: ${nic}`);
        this.nic = nic;
        this.isDataAvailable = true;
      }
    );
  }

  private selectIp(){
    return this.route.paramMap.subscribe(params => {
      var nicId = Number(params.get("id"));
      console.log(`Got address: ${nicId}`);
      this.selectedNic = nicId;
    })
  }

}
