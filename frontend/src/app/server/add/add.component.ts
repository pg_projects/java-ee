import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Nic } from 'src/app/nic/nic';
import { Server } from '../server';
import { ServerService } from '../server.service';
import { NicService } from 'src/app/nic/nic.service';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  availableNics:Nic[];
  serverForm: FormGroup;
  selectedNics:Nic[] = [];
  dataIsAvailable:boolean = false;

  private server: Server;
  private id: number
  private editMode: boolean = false;

  constructor(
    private serverService: ServerService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private nicService: NicService) {}

  ngOnInit() {
    this.getId();
    this.getAvailableNics();
    this.initForm();

    if(this.editMode)
        this.serverService.get(this.id).subscribe( server => {
        this.updateForm(server);
      });

      this.dataIsAvailable = true;
  }

  save() {
    this.server = new Server(this.serverForm.value);

    this.server.loading = parseFloat(this.serverForm.value['load'])

    if(this.editMode) {
      this.server.id = this.id;

      if(this.server.nics == [])
        this.server.nics = null;

      this.serverService.edit(this.server);
    }
    else {
      this.serverService.save(this.server);
    }

    this.router.navigate(['/servers/all']);
  }

  private initForm() {

    this.serverForm = this.fb.group(
      {
        name: ['', Validators.required],
        creationDate: ['', Validators.required],
        place: ['', Validators.required],
        owner: ['', Validators.required],
        cpu: ['', Validators.required],
        load: ['', Validators.required],
        nics: [null]
      });
  }
  private updateForm(server:Server = new Server()) {
    this.serverForm = this.fb.group(
    {
      name: [`${server.name}`, Validators.required],
      creationDate: [`${server.creationDate}`, Validators.required],
      place: [`${server.place}`, Validators.required],
      owner: [`${server.owner}`, Validators.required],
      cpu: [`${server.cpu}`, Validators.required],
      load: [`${server.loading}`, Validators.required],
      nics: [`${server.nics}`]
    });
  }

  private getId() {
      return this.route.paramMap.subscribe(params => {
        var id = params.get("id");

        if(id) {
          this.id = Number(id);
          this.editMode = true;
        }
      });
    }

  public getAvailableNics() {
    this.nicService.getAll().subscribe( nics =>
      {
        this.availableNics = nics
      });
  }
}
