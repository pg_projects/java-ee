import { Component, OnInit } from '@angular/core';
import { ServerService } from '../server.service';
import { Server } from '../server';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  servers:Server[];
  isDataAvailable:boolean = false;

  constructor(private serverService:ServerService, private router: Router) { }

  ngOnInit() {
    this.fillTable();
  }

  private fillTable() {
    this.serverService.getAll()
      .subscribe(servers => {
        this.servers = servers;
        this.isDataAvailable = true;
      }
    );
  }

  delete(id:number) {
    this.serverService.remove(id);
    this.servers = this.servers.filter(server => server.id !== id);
    this.router.navigate(['/servers/all']);
  }
}
