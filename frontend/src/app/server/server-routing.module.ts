import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { AllComponent } from './all/all.component';
import { ShowComponent } from './show/show.component';


const routes: Routes = [
  { path: 'servers/add', component: AddComponent },
  { path: 'servers/all', component: AllComponent },
  { path: 'servers/edit/:id', component: AddComponent },
  { path: 'servers/show/:id', component: ShowComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServerRoutingModule { }
