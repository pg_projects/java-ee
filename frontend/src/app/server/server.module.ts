import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServerRoutingModule } from './server-routing.module';
import { AllComponent } from './all/all.component';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AllComponent,
    AddComponent,
    ShowComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    ServerRoutingModule
  ]
})
export class ServerModule { }
