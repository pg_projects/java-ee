import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Server } from './server';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  constructor(private http: HttpClient) {
  }
      private api_url:string = 'http://backend:8080/+java-ee+/api/servers/';

      getAll(): Observable<Server[]> {
      var url = `${this.api_url}all`
      return this.http.get<Server[]>(url);
    }

    get(id:number): Observable<Server>{
      var url = `${this.api_url}${id}`

      return this.http.get<Server>(url);
    }

    edit(server:Server): void {
      var url = `${this.api_url}edit/`

      this.http.put(url, server)
        .subscribe((response: Response) =>{
        });
    }

    remove(id:number): void {
      var url = `${this.api_url}${id}`

      this.http.delete(url)
        .subscribe((response: Response) =>{
        });
    }

    save(server:Server): void {
      var url = `${this.api_url}add/`

      this.http.post(url, server)
        .subscribe((response: Response) =>{
        });
    }
}
