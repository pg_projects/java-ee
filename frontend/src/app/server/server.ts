import { Nic } from '../nic/nic';

export class Server {
  id:number;
  name:string = '';
  creationDate:Date = new Date();
  place:string = '';
  owner:string = '';
  cpu:number = 0;
  loading:number = 0;
  nics:Nic[] = [];

  constructor(server?: Partial<Server>){
    if(server)
      Object.assign(this, server);
  }
}