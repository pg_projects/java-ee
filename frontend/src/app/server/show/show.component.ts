import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServerService } from '../server.service';
import { Server } from '../server';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

constructor(private serverService: ServerService, private route: ActivatedRoute) { }
  server:Server;
  selectedServer:number;
  isDataAvailable:boolean = false;

  ngOnInit() {
    this.selectServer()
    this.fillTable()
  }

  private fillTable() {
    this.serverService.get(this.selectedServer)
      .subscribe(server => {
        this.server = server;
        this.isDataAvailable = true;
      }
    );
  }

  private selectServer(){
    return this.route.paramMap.subscribe(params => {
      var serverId = Number(params.get("id"));
      this.selectedServer = serverId;
    })
  }

}
